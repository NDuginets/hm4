window.onload = function() {
  var OurMenu = [];
  var OurOrders = [];
  const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
  ];


  let mainBurger = {
    cookingTime: 0,
    showComposition: function(){
      let {composition, name} = this;
      let compositionLength = composition.length;
      console.log(compositionLength);
      if( compositionLength !== 0){
        composition.map( function( item ){
            console.log( 'Состав бургера', name, item );
        })
      }
    }
  
}


function Burger( name, ingredients, cookingTime){
this.name = name;
if (cookingTime) {
  this.cookingTime = cookingTime;
}
this.composition = ingredients;

Object.setPrototypeOf(this,mainBurger); 

}


let mainOrder = {
id: 1,
orderNumber: 1,
orderBurger: "Базовый",
orderException: "---",
orderAvailability: "---"
}

function Order(name, condition, value){
//this.name = name;
Object.setPrototypeOf(this,mainOrder);
this.searchBurger = function() {
    if (name) {
    this.orderBurger = OurMenu.find(function(item){
      return item.name == name; 
    })
    }

else if (condition && value) {
  if (condition == "has") {
    this.orderBurger = OurMenu.find(function(item){
      let state = false;
      value.every(function(itemValue){
       state = item.composition.indexOf(itemValue) > -1;
       return state;
      });
      return state;
    });
   this.orderAvailability = value;
  }


  if (condition == "except") {
    this.orderBurger = OurMenu.find(function(item){
      let state = false;
      value.every(function(itemValue){
       state = item.composition.indexOf(itemValue) == -1;
       return state;
      });
      return state;
    })
    this.orderException = value;
  }
}

}
this.id = OurOrders.length + 1;
this.orderNumber = OurOrders.length + 1;

}
//создание обработчиков
  document.getElementById("addToOrder").addEventListener("click",addOrder);
  document.getElementById("addToMenu").addEventListener("click",addBurger);
  //рендер масива ингридов
  Ingredients.forEach(addLi);
  function addLi (item){
    var newLi = document.createElement('li');
    newLi.innerHTML = '<input type="checkbox" value="'+ item + '"> <label>'+item + '</label>';
    document.getElementById("ingridients").appendChild(newLi);
  }

//добавление бургера
function addBurger(){
  var name = document.getElementById("uname").value;
  var cookingTime = document.getElementById("cookingtime").value;
  var arrCheckbox = document.querySelectorAll('input[type="checkbox"]');
  var ingredients = [];
  
  arrCheckbox.forEach(function(item){
  if (item.checked) {
    ingredients.push(item.value);
    
  }
  })
  var burgerOne = new Burger(name,ingredients,cookingTime);
  OurMenu.push(burgerOne);
//рендер бургера
  var targetField = document.getElementById("menu");
  var pasteElem = document.createElement('div');
  pasteElem.innerHTML = `<p>${burgerOne.name}</p><p>Время приготовления:${burgerOne.cookingTime}</p><p>Состав:${burgerOne.composition.join(', ')}`;
  targetField.appendChild(pasteElem);
  
};
//добавление заказа
function addOrder(){
let name = document.getElementById("uorder").value;
let condition = document.getElementById("condition").value;
let arrCheckbox = document.querySelectorAll('input[type="checkbox"]');
let ingredients = [];

arrCheckbox.forEach(function(item){
if (item.checked) {
  ingredients.push(item.value);
  
}
})

let orderOne = new Order(name,condition,ingredients);
orderOne.searchBurger();
if (orderOne.orderBurger) {
  OurOrders.push(orderOne);
  console.log(OurOrders);
  renderOrder(orderOne);
} else {
  let randomBurger = OurMenu[Math.floor(Math.random()*OurMenu.length)];
  if (confirm(`Такого бургера нету, хотите по пробовать ${randomBurger.name}`)) {
    orderOne.orderBurger = randomBurger;
    if (orderOne.orderAvailability) {
      orderOne.orderAvailability = "---";
    } else if (orderOne.orderException) {
      orderOne.orderException = "---";
    };
    OurOrders.push(orderOne);
   console.log(orderOne);
    renderOrder(orderOne);
  }
}


}
//рендер заказа
function renderOrder(orderOne){
  let targetField = document.getElementById("order");
  let pasteElem = document.createElement('div');
  pasteElem.innerHTML = `<p>Заказ:${orderOne.id} Бургер:${orderOne.orderBurger.name}, будет готов через ${orderOne.orderBurger.cookingTime} </p>`;
  targetField.appendChild(pasteElem);
}
}